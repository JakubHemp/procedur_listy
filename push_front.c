#include "moduly.h"

void push_front(struct Client **head, char *p_surname) 
{
    struct Client *new;
    new = (struct Client*)malloc(sizeof(struct Client));
    
    strcpy(new->surname, p_surname);
    new->next = *head; // change
    *head = new; // new element is a head now
   
    return;
}