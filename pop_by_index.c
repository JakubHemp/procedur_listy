#include "moduly.h"
#include <stdio.h>

void pop_by_index(struct Client **head, int p_index)
{
	if(p_index == 0)
		pop_front(head);
	else
	{
		struct Client *somebody = *head;
		struct Client *somebody_else;

		int i = 0;
		while(somebody->next != NULL && i<p_index-1) // untill we reach end of the list or right index
		{
			somebody = somebody->next;
			i++;
		}
		somebody_else = somebody->next;
		somebody->next = somebody_else->next;
		free(somebody_else);
	}
		
	return;
}