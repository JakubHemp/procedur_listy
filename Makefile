CC=gcc
CFLAGS=-Wall
LIBS=-lm

zad11: zad11.o list_size.o show_list.o push_front.o push_back.o push_by_index.o pop_front.o pop_by_index.o pop_by_surname.o pop_back.o
	$(CC) $(CFLAGS) -o zad11 zad11.o list_size.o show_list.o push_front.o push_back.o push_by_index.o pop_front.o pop_by_index.o pop_by_surname.o pop_back.o $(LIBS)

zad11.o: zad11.c moduly.h
	$(CC) $(CFLAGS) -c zad11.c

list_size.o: list_size.c
	$(CC) $(CFLAGS) -c list_size.c

show_list.o: show_list.c
	$(CC) $(CFLAGS) -c show_list.c

push_front.o: push_front.c
	$(CC) $(CFLAGS) -c push_front.c

push_back.o: push_back.c
	$(CC) $(CFLAGS) -c push_back.c

push_by_index.o: push_by_index.c
	$(CC) $(CFLAGS) -c push_by_index.c

pop_front.o: pop_front.c
	$(CC) $(CFLAGS) -c pop_front.c

pop_by_index.o: pop_by_index.c
	$(CC) $(CFLAGS) -c pop_by_index.c

pop_by_surname.o: pop_by_surname.c
	$(CC) $(CFLAGS) -c pop_by_surname.c

pop_back.o: pop_back.c
	$(CC) $(CFLAGS) -c pop_back.c

