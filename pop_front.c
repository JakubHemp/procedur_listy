#include "moduly.h"
#include <stdio.h>

void pop_front(struct Client **head) 
{
    struct Client *somebody = NULL;

    if(*head != NULL) // head has to exist if we want to delete it
    {
        somebody = (*head)->next; // hold new head in somebody
        free(*head); // delete old head
        *head = somebody; // new head
    }

    return;
}
