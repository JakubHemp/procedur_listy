#include "moduly.h"
#include <stdio.h>

void push_back(struct Client **head, char *p_surname)
{ 
    // list is empty, we create head
    if(*head == NULL)
    {
        *head = (struct Client*)malloc(sizeof(struct Client));
        strcpy((*head)->surname, p_surname);
        (*head)->next = NULL;
    }
    else // we create new element and go through whole list to add it at the end of list
    {
        struct Client *new = *head; // we create new list element and assign it to head

        // we set new elem to the last elem of list 
        while(new->next != NULL)
        {
            new = new->next;
        }
        // allocate memory for new elem after the last one (new tail)
        new->next = (struct Client*)malloc(sizeof(struct Client));    

        // assign values to the element after the last from the list (to new tail)
        strcpy(new->next->surname, p_surname);  
        new->next->next = NULL;    
    }    

    return;
}
