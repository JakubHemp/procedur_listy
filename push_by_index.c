#include "moduly.h"

void push_by_index(struct Client **head, char *p_surname, int p_index)
{
    if(p_index == 0)
        push_front(head, p_surname);
    else
    {
        if(p_index == list_size(*head))
            push_back(head, p_surname);
        else
        {
            struct Client *new = *head;
            struct Client *somebody; // auxiliary struct variable

            int i = 0;

            while(new->next != NULL && i<p_index-1)
            {
                new = new->next;
                i++;
            }

            somebody = new->next;
            new->next = (struct Client*)malloc(sizeof(struct Client));
            strcpy(new->next->surname, p_surname);
            new->next->next = somebody;
        }
    }
        
    return;
}