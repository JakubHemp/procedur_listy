#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "moduly.h"

int wczytajdane();

int main()
{
    int choice = 0;
    char surname[20] = "";

    char random_name[] = {"Jacu"};

    struct Client *head;
    head = (struct Client*)malloc(sizeof(struct Client));
    strcpy(head->surname, random_name);
    head->next = NULL;

    while(1)
    {
        printf("Select options \n");
        choice = wczytajdane();

        switch(choice)
        {
            case 1:
                printf("Enter new client: ");
                pop_back();
            break;
            
            case 2:
                surname[0] = '\0';
                printf("Enter client to remove: ");
                pop_by_surname();
            break;

            case 3:
                list_size();
                break;

            case 4:
                list_show();
                break;
            
            case 5:
                printf("Do you want close program(press 1)");
                int yes = 1;
                scanf("%d", &yes);
                if (yes == 1)
                {
                    exit(0);
                }    
                break;
        }

    }

    return 0;
}

int wczytajdane()
{
    int zmienna;
    while (scanf("%d", &zmienna) != 1)
    {
        printf("it is forbidden to enter characters other than numbers");
        int c;
        while ((c = getchar()) != '\n' && c != EOF);
    }
    return zmienna;
}