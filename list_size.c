#include "moduly.h"
#include <stdio.h>

int list_size(struct Client *head)
{
    int size = 0;
    if(head == NULL)
        return size;
    else
    {
        struct Client *somebody = head;
        // go through the whole list and count its elements, until you reach last element (which has next attribute on NULL)
        do
        {
            size++;
            somebody = somebody->next;
        } while (somebody != NULL);        
    }
    
    return size;
}